variable "gitlab_token" {
 type = "string"
}

provider "gitlab" {
    token = "${var.gitlab_token}"
    base_url  = "https://gitlab.rebrainme.com/"
}

resource "gitlab_project" "terraform-project" {
  name        = "terraform-project"
  description = "Terraform Gitlab Project"
   visibility_level = "private"
}


resource "gitlab_deploy_key" "terraform-project" {
  project = "samir/terraform-project"
  title   = "Deploy key"
  can_push = "true"
  key     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCYU4zSxyd44ZwbZM2/KuaR7IoQXIAuhTTkGMKRv32DNJy48VMuykhIFpUmnnRV1aFvstAvVVB45yTN9dUoNR+cay+R3ITA/+1D8h3lmtotf72Bz21WAclwfgEj+VzyIWi4UDVi79nrCOpuamYfcw5Z300FdayZu9NVBQbBgRHa5W/MfgjNPHAWC0WbNKaNMI8VBec83CmwcTs5yUwsOMMopraai3LXn9zTL/gTAaYHpJ60CPXlrKgttoZy6uur2xcgtHGrAumeB841jJRRfSg/h3CvF4vYwMphfBSuMdyLFdxXGekuQUp/BC3F0GsyM17LIPX4CyxxbmY4VLe9mqZ9"
}
